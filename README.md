# Keysight 33500x Waveform Generator EPICS IOC

## PV List

### Common PVs

The following PVs deal with the generator system and are not bound to a specific output channel.

The prefix for these PVs should be `$(DEVICE)` (e.g `LAB:WFG-003:`)

| Common PVs       | Description                                        | Options                  |
|------------------|----------------------------------------------------|--------------------------|
| DISP:TEXT        | Display a 40 char long string on the device screen | -                        |
| DISP:CLEAR       | Clear display message                              |                          |
| DISP:CTL         | Display Control                                    | ON \| OFF                |
| DISP:ARBUNIT     | Set rate unit for arbitrary waveforms              | SRAT \| FREQ \| PER      |
| DISP:ARBUNIT:RBV | Read rate unit for arbitrary waveforms             | SRAT \| FREQ \| PER      |
| UNIT:ANGLE       | Set angle unit                                     | DEG \| RAD \| SEC \| DEF |
| UNIT:ANGLE:RBV   | Read angle unit                                    | DEG \| RAD \| SEC \| DEF |

### Channel specific PVs

The following PVs control the output channels and are replicated for each channel.

The prefix for these PVs should be `$(DEVICE):$(SOURCE):`, with `$(SOURCE)` being the output number (e.g `LAB:WFG-003:1:`)

| PVs              | Description                                                              | Options                                                        |
|------------------|--------------------------------------------------------------------------|----------------------------------------------------------------|
| DISABLE          | Disable communication with the channel                                   |                                                                |
| MEMCLEAR         | Clear volatile memory (will clear all loaded waveforms)                  |                                                                |
| FUNC             | Select waveform function                                                 | SIN \| SQU \| TRI \| RAMP \| PULS \| PRBS \| NOIS \| ARB \| DC |
| FUNC:RBV         | Read waveform function                                                   | SIN \| SQU \| TRI \| RAMP \| PULS \| PRBS \| NOIS \| ARB \| DC |
| UNIT:VOLTAGE     | Select the method for specifying voltage ranges                          | Vpp \| Vrms \| dBm                                             |
| UNIT:VOLTAGE:RBV | Read the method for specifying voltage ranges                            | Vpp \| Vrms \| dBm                                             |
| PULSEW           | Set pulse width [s]                                                      |                                                                |
| PULSEW:RBV       | Read pulse width [s]                                                     |                                                                |
| PULSETRAN        | Set pulse edge transition time [s]                                       |                                                                |
| PULSETRAN:RBV    | Set pulse edge transition time [s]                                       |                                                                |
| OUT              | Output control                                                           | OFF(0) \| ON(1)                                                |
| OUT:RBV          | Read output status                                                       | OFF(0) \| ON(1)                                                |
| PHASE            | Set phase angle [unit according to UNIT:ANGLE]                           |                                                                |
| PHASE:MIN        | Set phase angle MIN value [unit according to UNIT:ANGLE]                 |                                                                |
| PHASE:MAX        | Set phase angle MAX value [unit according to UNIT:ANGLE]                 |                                                                |
| PHASE:RBV        | Read current phase angle [unit according to UNIT:ANGLE]                  |                                                                |
| PHASE:PREDEF     | Set phase angle to a predefined value                                    | MINIMUM \| MAXIMUM \| DEFAULT                                  |
| FREQ             | Set output frequency [Hz].                                               |                                                                |
| FREQ:MIN         | Set predefined MIN frequency [Hz]                                        |                                                                |
| FREQ:MAX         | Set predefined MAX frequency [Hz]                                        |                                                                |
| FREQ:RBV         | Read output frequency [Hz]                                               |                                                                |
| FREQ:PREDEF      | Set frequency to a predefined value                                      | MINIMUM \| MAXIMUM \| DEFAULT                                  |
| AMP              | Set output amplitude [unit according to UNIT:VOLTAGE]                    |                                                                |
| AMP:MIN          | Set predefined MIN amplitude [unit according to UNIT:VOLTAGE]            |                                                                |
| AMP:MAX          | Set predefined MAX amplitude [unit according to UNIT:VOLTAGE]            |                                                                |
| AMP:RBV          | Read output amplitude [unit according to UNIT:VOLTAGE]                   |                                                                |
| AMP:PREDEF       | Set amplitude to a predefined value                                      | MINIMUM \| MAXIMUM \| DEFAULT                                  |
| OFFSET           | Set output DC offset [V]                                                 |                                                                |
| OFFSET:MIN       | Set predefined MIN DC offset [V]                                         |                                                                |
| OFFSET:MAX       | Set predefined MAX DC offset [V]                                         |                                                                |
| OFFSET:RBV       | Read output amplitude DC offset [V]                                      |                                                                |
| OFFSET:PREDEF    | Set amplitude DC offset to a predefined value                            | MINIMUM \| MAXIMUM \| DEFAULT                                  |
| ARBFILENAME      | Select name to an Arbitrary waveform file                                |                                                                |
| ARBWAVENAME      | Select loaded arbitrary waveform file to output                          |                                                                |
| ARBWAVENAME:RBV  | Read current arbitrary waveform file                                     |                                                                |
| ARBSAMPRATE      | Set arbitrary waveform sampling rate [Sa/s]                              |                                                                |
| ARBSAMPRATE:RBV  | Read arbitrary waveform sampling rate [Sa/s]                             |                                                                |
| ARBFREQ          | Set arbitrary waveform frequency [Hz]                                    |                                                                |
| ARBFREQ:RBV      | Read arbitrary waveform frequency [Hz]                                   |                                                                |
| ARBPERIOD        | Set arbitrary waveform period [s]                                        |                                                                |
| ARBPERIOD:RBV    | Read arbitrary waveform period [s]                                       |                                                                |
| ARBPEAKV         | Set arbitrary waveform peak-to-peak voltage [V]                          |                                                                |
| ARBPEAKV:RBV     | Read arbitrary waveform peak-to-peak voltage [V]                         |                                                                |
| ARBFILT          | Set arbitrary waveform sample filter setting                             | OFF \| STEP \| NORM                                            |
| ARBFILT:RBV      | Read arbitrary waveform sample filter setting                            | OFF \| STEP \| NORM                                            |
| ARBSYNC          | Synchronize arbitrary waveform on both channels                          |                                                                |
| ARBDATA_DAC      | Arbitrary waveform data points [DAC counts]. Range from -32767 to +32767 |                                                                |
| ARBDATA          | Arbitrary waveform data points [float]. Range from -1.0 to +1.0          |                                                                |
| ARBPOINTS:RBV    | Read point count on the currently loaded arbitrary waveform              |                                                                |

## Usage

### Arbitrary wave guide

The arbitrary waveform setting is a bit more complex than the predefined ones and the following steps must be performed sequentially, in order to prevent any errors.

  1. Set the new wave name on `ARBFILENAME` PV. Note that this name must be unique and up to 30-chars long. If there's a wave with the same name already loaded on the WFG, it will return an error an refuse the new data. In this case, one can set the `MEMCLEAR` PV before, which will erase all volatile loaded in the generator.

  2. a) Send wave points in floating points format (relative to maximum amplitude) to `ARBDATA` PV.  
     b) Send wave points in integer format (DAC counts) to `ARBDATA_DAC`.

  3. Load the waveform to the channel, writing its name (the same from step 1) to the `ARBWAVENAME` PV.

  4. Set the Sampling rate (`ARBSAMPRATE`), or wave frequency (`ARBFREQ`), or wave period (`ARBPERIOD`). These three PVs are adjusted according to the one that has been set.

  5. Set the peak-to-peak voltage (`ARBPEAKV`).

  6. Select the `ARB` function on the generator and enable the output

Note that the maximum sampling rate is different among the 33500 series and is also affected by the filtering options and sample count, so some values may not be achievable.
Similar restrictions applies to the output amplitude, so it will have lower limits for 50 Ohm loads than HighZ.

## Examples

On the following examples, assume ``$(DEVICE) = LAB:WFG-003``.

### Sine wave

Setting a **10kHz**, **500mVpp** amplitude, **90 degrees** phase offset sine wave on **output 1**:

```
  caput LAB:WFG-003:1:OUT Off &&\
  caput LAB:WFG-003:UNIT:ANGLE DEG &&\
  caput LAB:WFG-003:1:UNIT:VOLTAGE Vpp &&\
  caput LAB:WFG-003:1:FUNC SIN &&\
  caput LAB:WFG-003:1:FREQ 10e3 &&\
  caput LAB:WFG-003:1:AMP 0.5 &&\
  caput LAB:WFG-003:1:PHASE 90 &&\
  caput LAB:WFG-003:1:OFFSET 0 &&\
  caput LAB:WFG-003:1:OUT On
```

Note that both \*UNIT\* commands are used to ensure that the phase and amplitude values are set with the units we want, since the previous state of the generator is assumed unknown in this example.

### Pulse

Setting a pulse **13.5ms** long, with **1Vrms** amplitude, **1us** rising and falling edge, at **14Hz** on **output 2**:

```
  caput LAB:WFG-003:2:OUT Off &&\
  caput LAB:WFG-003:2:UNIT:VOLTAGE Vrms &&\
  caput LAB:WFG-003:2:FUNC PULS &&\
  caput LAB:WFG-003:2:FREQ 14.0 &&\
  caput LAB:WFG-003:2:PULSEW 13.5e-3 &&\
  caput LAB:WFG-003:2:PULSETRAN 1e-6 &&\
  caput LAB:WFG-003:2:AMP 0.4 &&\
  caput LAB:WFG-003:2:PHASE 0 &&\
  caput LAB:WFG-003:2:OFFSET 0 &&\
  caput LAB:WFG-003:2:OUT On
```

### Arbitrary wave

Configuring a custom ramp with 10% amplitude steps (11 points), **100mVpp** amplitude, repeating at **100Hz**, without point filtering.

```
  caput LAB:WFG-003:1:OUT Off &&\
  caput LAB:WFG-003:1:ARBFILENAME test_ramp &&\
  caput -a LAB:WFG-003:1:ARBDATA 11 0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 &&\
  caput LAB:WFG-003:1:ARBWAVENAME test_ramp &&\
  caput LAB:WFG-003:1:ARBFREQ 100 &&\
  caput LAB:WFG-003:1:UNIT:VOLTAGE Vpp &&\
  caput LAB:WFG-003:1:AMP 0.1 &&\
  caput LAB:WFG-003:1:FUNC ARB &&\
  caput LAB:WFG-003:1:OUT On
```

Configuring a custom ramp with "random" DAC values, **100mVpp** amplitude, repeating at **10Hz**, without point filtering.

```
  caput LAB:WFG-003:1:OUT Off &&\
  caput LAB:WFG-003:1:ARBFILENAME dac_random &&\
  caput -a LAB:WFG-003:1:ARBDATA_DAC 10 0 13445 10556 -5667 24998 -32000 400 31484 136 -8871 &&\
  caput LAB:WFG-003:1:ARBWAVENAME dac_random &&\
  caput LAB:WFG-003:1:ARBFREQ 100 &&\
  caput LAB:WFG-003:1:UNIT:VOLTAGE Vpp &&\
  caput LAB:WFG-003:1:AMP 0.1 &&\
  caput LAB:WFG-003:1:FUNC ARB &&\
  caput LAB:WFG-003:1:OUT On
```

**IMPORTANT NOTE:**

When sending the data to the waveform PV `ARBDATA` or `ARBDATA_DAC` using `caput`, you have to specify the flag `-a` to indicate that it should consume the entire array provided, not only the first value. When using this flag, the first argument must be the number of elements to be sent, for example:
```
    caput -a <PV> <array count> [array]
    caput -a LAB:WFG-003:1:ARBDATA_DAC 10 1 2 3 4 5 6 7 8 9 10
```
