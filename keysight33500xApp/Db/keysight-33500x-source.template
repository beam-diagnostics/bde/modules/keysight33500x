record(longin, "$(DEVICE):$(SOURCE):iSrcConnectEvent") {
  field(DESC, "Detect (re)connections")
  field(INP,  "$(DEVICE):iConnectEvent CP")
  field(FLNK, "$(DEVICE):$(SOURCE):iSRCINIT")
  field(DISV, "0")
  field(SDIS, "$(DEVICE):iConnectEvent")
}


record(fanout, "$(DEVICE):$(SOURCE):iSRCINIT") {
  field(DESC, "Initialize readback parameters")
  field(LNK1, "$(DEVICE):$(SOURCE):iSRCUPDATE.PROC CA")
  field(LNK2, "$(DEVICE):$(SOURCE):PHASE:MIN")
  field(LNK3, "$(DEVICE):$(SOURCE):FREQ:MIN")
  field(LNK4, "$(DEVICE):$(SOURCE):AMP:MIN")
  field(LNK5, "$(DEVICE):$(SOURCE):OFFSET:MIN")
  field(LNK6, "$(DEVICE):$(SOURCE):UNIT:VOLTAGE")
}


record(seq, "$(DEVICE):$(SOURCE):iSRCUPDATE") {
  field(SCAN, "5 second")
  field(DESC, "Update Params")
  field(LNK1, "$(DEVICE):$(SOURCE):FUNC:RBV.PROC")
  field(LNK2, "$(DEVICE):$(SOURCE):FREQ:RBV.PROC")
  field(LNK3, "$(DEVICE):$(SOURCE):AMP:RBV.PROC")
  field(LNK4, "$(DEVICE):$(SOURCE):OFFSET:RBV.PROC")
#  field(LNK5, "$(DEVICE):$(SOURCE):PULSEW:RBV.PROC")
#  field(LNK6, "$(DEVICE):$(SOURCE):TRAN:RBV.PROC")
  field(LNK7, "$(DEVICE):$(SOURCE):PHASE:RBV.PROC")
#  field(LNKA, "$(DEVICE):$(SOURCE):iSRCUPDATE2.PROC")
}

record(seq, "$(DEVICE):$(SOURCE):iSRCUPDATE2") {
  field(DESC, "Update Params 2")
  field(DOL1, "1")
  field(DOL2, "1")
  field(DOL3, "1")
  field(DOL4, "1")
}

record(bo, "$(DEVICE):$(SOURCE):DISABLE") {
  field(DESC, "Disable comms")
  field(VAL,  "0")
  field(ZNAM, "Enabled")
  field(ONAM, "Disabled")
}




#
# FUNCTION
#
record(mbbo, "$(DEVICE):$(SOURCE):FUNC") {
  field(DESC, "Function")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):iFUNC-FO")
  field(OUT,  "@keysight-33500x.proto src_write_string($(SOURCE),FUNC) $(PORT)")
  field(ZRST, "SIN")
  field(ONST, "SQU")
  field(TWST, "TRI")
  field(THST, "RAMP")
  field(FRST, "PULS")
  field(FVST, "PRBS")
  field(SXST, "NOIS")
  field(SVST, "ARB")
  field(EIST, "DC")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(fanout, "$(DEVICE):$(SOURCE):iFUNC-FO") {
  field(DESC, "Update parameters on function change")
  field(LNK1, "$(DEVICE):$(SOURCE):iSRCUPDATE")
#The frequency range changes with the function
  field(LNK2, "$(DEVICE):$(SOURCE):FREQ:MIN")
#The amplitude range changes with the function
  field(LNK3, "$(DEVICE):$(SOURCE):AMP:MIN")
}


record(mbbi, "$(DEVICE):$(SOURCE):FUNC:RBV") {
  field(DESC, "Function Readback")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_string($(SOURCE),FUNC) $(PORT)")
  field(ZRST, "SIN")
  field(ONST, "SQU")
  field(TWST, "TRI")
  field(THST, "RAMP")
  field(FRST, "PULS")
  field(FVST, "PRBS")
  field(SXST, "NOIS")
  field(SVST, "ARB")
  field(EIST, "DC")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(ao, "$(DEVICE):$(SOURCE):PULSEW") {
  field(DESC, "Pulse Width")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):PULSEW:RBV")
  field(OUT,  "@keysight-33500x.proto src_write_float($(SOURCE),FUNC:PULS:WIDT) $(PORT)")
  field(PREC, "6")
  field(EGU,  "sec")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}

record(ai, "$(DEVICE):$(SOURCE):PULSEW:RBV") {
  field(DESC, "Pulse Width Readback")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_float($(SOURCE),FUNC:PULS:WIDT) $(PORT)")
  field(PREC, "6")
  field(EGU,  "sec")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}

record(ao, "$(DEVICE):$(SOURCE):PULSETRAN") {
  field(DESC, "Pulse Edge Transition")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):PULSETRAN:RBV")
  field(OUT,  "@keysight-33500x.proto src_write_float($(SOURCE),FUNC:PULS:TRAN) $(PORT)")
  field(PREC, "3")
  field(EGU,  "s")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}

record(ai, "$(DEVICE):$(SOURCE):PULSETRAN:RBV") {
  field(DESC, "Pulse Edge Transition Readback")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_float($(SOURCE),FUNC:PULS:TRAN) $(PORT)")
  field(PREC, "3")
  field(EGU,  "s")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}




#
# PHASE
#
record(ao, "$(DEVICE):$(SOURCE):PHASE") {
  field(DESC, "Phase Angle")
  field(PREC, "3")
  field(EGU,  "deg")
  field(OUT,  "$(DEVICE):$(SOURCE):iPHASE PP")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(stringout, "$(DEVICE):$(SOURCE):iPHASE") {
  field(DESC, "Phase Angle")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):PHASE:RBV")
  field(OUT,  "@keysight-33500x.proto src_write_string($(SOURCE),PHAS) $(PORT)")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(mbbo, "$(DEVICE):$(SOURCE):PHASE:PREDEF") {
  field(DESC, "Predefined Phase Angle")
  field(ZRST, "MINIMUM")
  field(ONST, "MAXIMUM")
  field(TWST, "DEFAULT")
  field(FLNK, "$(DEVICE):$(SOURCE):PHASE:iPREDEF2STR")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(stringout, "$(DEVICE):$(SOURCE):PHASE:iPREDEF2STR") {
  field(DESC, "Convert mbbo to string")
  field(DOL,  "$(DEVICE):$(SOURCE):PHASE:PREDEF")
  field(OUT,  "$(DEVICE):$(SOURCE):iPHASE PP")
  field(OMSL, "closed_loop")
}


record(ai, "$(DEVICE):$(SOURCE):PHASE:MIN") {
  field(DESC, "Minimum Phase Angle")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):PHASE:MAX")
  field(INP,  "@keysight-33500x.proto src_read_float_param($(SOURCE),PHAS,MIN) $(PORT)")
  field(PREC, "3")
  field(EGU,  "deg")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(ai, "$(DEVICE):$(SOURCE):PHASE:MAX") {
  field(DESC, "Maximum Phase Angle")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):PHASE:iSETMINMAX")
  field(INP,  "@keysight-33500x.proto src_read_float_param($(SOURCE),PHAS,MAX) $(PORT)")
  field(PREC, "3")
  field(EGU,  "deg")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(seq, "$(DEVICE):$(SOURCE):PHASE:iSETMINMAX") {
  field(DESC, "Set phase PV parameters")
  field(DOL1, "$(DEVICE):$(SOURCE):PHASE:MIN")
  field(LNK1, "$(DEVICE):$(SOURCE):PHASE.DRVL")
  field(DOL2, "$(DEVICE):$(SOURCE):PHASE:MIN")
  field(LNK2, "$(DEVICE):$(SOURCE):PHASE.LOPR")
  field(DOL3, "$(DEVICE):$(SOURCE):PHASE:MAX")
  field(LNK3, "$(DEVICE):$(SOURCE):PHASE.DRVH")
  field(DOL4, "$(DEVICE):$(SOURCE):PHASE:MAX")
  field(LNK4, "$(DEVICE):$(SOURCE):PHASE.HOPR")
}


record(ai, "$(DEVICE):$(SOURCE):PHASE:RBV") {
  field(DESC, "Phase Angle Readback")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_float($(SOURCE),PHAS) $(PORT)")
  field(PREC, "3")
  field(EGU,  "deg")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(sseq, "$(DEVICE):$(SOURCE):iUPDATEPHASE") {
  field(DESC, "Update phase on angle unit change")
  field(DOL1, "$(DEVICE):UNIT:ANGLE:RBV CP")
  field(DOL2, "$(DEVICE):UNIT:ANGLE:RBV")
  field(DOL3, "$(DEVICE):UNIT:ANGLE:RBV")
  field(DOL4, "$(DEVICE):UNIT:ANGLE:RBV")

  field(LNK1, "$(DEVICE):$(SOURCE):PHASE.EGU")
  field(LNK2, "$(DEVICE):$(SOURCE):PHASE:MIN.EGU PP")
  field(LNK3, "$(DEVICE):$(SOURCE):PHASE:MAX.EGU")
  field(LNK4, "$(DEVICE):$(SOURCE):PHASE:RBV.EGU PP")
}




#
# FREQUENCY
#
record(ao, "$(DEVICE):$(SOURCE):FREQ") {
  field(DESC, "Frequency")
  field(PREC, "3")
  field(EGU,  "Hz")
  field(OUT,  "$(DEVICE):$(SOURCE):iFREQ PP")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(stringout, "$(DEVICE):$(SOURCE):iFREQ") {
  field(DESC, "Frequency")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):FREQ:RBV")
  field(OUT,  "@keysight-33500x.proto src_write_string($(SOURCE),FREQ) $(PORT)")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(mbbo, "$(DEVICE):$(SOURCE):FREQ:PREDEF") {
  field(DESC, "Predefined Frequency")
  field(ZRST, "MINIMUM")
  field(ONST, "MAXIMUM")
  field(TWST, "DEFAULT")
  field(FLNK, "$(DEVICE):$(SOURCE):FREQ:iPREDEF2STR")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(stringout, "$(DEVICE):$(SOURCE):FREQ:iPREDEF2STR") {
  field(DESC, "Convert mbbo to string")
  field(DOL,  "$(DEVICE):$(SOURCE):FREQ:PREDEF")
  field(OUT,  "$(DEVICE):$(SOURCE):iFREQ PP")
  field(OMSL, "closed_loop")
}


record(ai, "$(DEVICE):$(SOURCE):FREQ:MIN") {
  field(DESC, "Minimum Frequency")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):FREQ:MAX")
  field(INP,  "@keysight-33500x.proto src_read_float_param($(SOURCE),FREQ,MIN) $(PORT)")
  field(PREC, "3")
  field(EGU,  "Hz")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(ai, "$(DEVICE):$(SOURCE):FREQ:MAX") {
  field(DESC, "Maximum Frequency")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):FREQ:iSETMINMAX")
  field(INP,  "@keysight-33500x.proto src_read_float_param($(SOURCE),FREQ,MAX) $(PORT)")
  field(PREC, "3")
  field(EGU,  "Hz")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(seq, "$(DEVICE):$(SOURCE):FREQ:iSETMINMAX") {
  field(DESC, "Set frequency PV parameters")
  field(DOL1, "$(DEVICE):$(SOURCE):FREQ:MIN")
  field(LNK1, "$(DEVICE):$(SOURCE):FREQ.DRVL")
  field(DOL2, "$(DEVICE):$(SOURCE):FREQ:MIN")
  field(LNK2, "$(DEVICE):$(SOURCE):FREQ.LOPR")
  field(DOL3, "$(DEVICE):$(SOURCE):FREQ:MAX")
  field(LNK3, "$(DEVICE):$(SOURCE):FREQ.DRVH")
  field(DOL4, "$(DEVICE):$(SOURCE):FREQ:MAX")
  field(LNK4, "$(DEVICE):$(SOURCE):FREQ.HOPR")
}


record(ai, "$(DEVICE):$(SOURCE):FREQ:RBV") {
  field(DESC, "Frequency Readback")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_float($(SOURCE),FREQ) $(PORT)")
  field(PREC, "3")
  field(EGU,  "Hz")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}




#
# AMPLITUDE
#
record(ao, "$(DEVICE):$(SOURCE):AMP") {
  field(DESC, "Amplitude")
  field(PREC, "3")
  field(EGU,  "vpp")
  field(OUT,  "$(DEVICE):$(SOURCE):iAMP PP")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(stringout, "$(DEVICE):$(SOURCE):iAMP") {
  field(DESC, "Amplitude")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):AMP:RBV")
  field(OUT,  "@keysight-33500x.proto src_write_string($(SOURCE),VOLT) $(PORT)")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(mbbo, "$(DEVICE):$(SOURCE):AMP:PREDEF") {
  field(DESC, "Predefined Amplitude")
  field(ZRST, "MINIMUM")
  field(ONST, "MAXIMUM")
  field(TWST, "DEFAULT")
  field(FLNK, "$(DEVICE):$(SOURCE):AMP:iPREDEF2STR")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(stringout, "$(DEVICE):$(SOURCE):AMP:iPREDEF2STR") {
  field(DESC, "Convert mbbo to string")
  field(DOL,  "$(DEVICE):$(SOURCE):AMP:PREDEF")
  field(OUT,  "$(DEVICE):$(SOURCE):iAMP PP")
  field(OMSL, "closed_loop")
}


record(ai, "$(DEVICE):$(SOURCE):AMP:MIN") {
  field(DESC, "Minimum Amplitude")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):AMP:MAX")
  field(INP,  "@keysight-33500x.proto src_read_float_param($(SOURCE),VOLT,MIN) $(PORT)")
  field(PREC, "3")
  field(EGU,  "vpp")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(ai, "$(DEVICE):$(SOURCE):AMP:MAX") {
  field(DESC, "Maximum Amplitude")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):AMP:iSETMINMAX")
  field(INP,  "@keysight-33500x.proto src_read_float_param($(SOURCE),VOLT,MAX) $(PORT)")
  field(PREC, "3")
  field(EGU,  "vpp")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(seq, "$(DEVICE):$(SOURCE):AMP:iSETMINMAX") {
  field(DOL1, "$(DEVICE):$(SOURCE):AMP:MIN")
  field(LNK1, "$(DEVICE):$(SOURCE):AMP.DRVL")
  field(DOL2, "$(DEVICE):$(SOURCE):AMP:MIN")
  field(LNK2, "$(DEVICE):$(SOURCE):AMP.LOPR")
  field(DOL3, "$(DEVICE):$(SOURCE):AMP:MAX")
  field(LNK3, "$(DEVICE):$(SOURCE):AMP.DRVH")
  field(DOL4, "$(DEVICE):$(SOURCE):AMP:MAX")
  field(LNK4, "$(DEVICE):$(SOURCE):AMP.HOPR")
}


record(ai, "$(DEVICE):$(SOURCE):AMP:RBV") {
  field(DESC, "Amplitude Readback")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_float($(SOURCE),VOLT) $(PORT)")
  field(PREC, "3")
  field(EGU,  "vpp")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(mbbi, "$(DEVICE):$(SOURCE):UNIT:VOLTAGE:RBV") {
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_string($(SOURCE),VOLT:UNIT) $(PORT)")
  field(ZRST, "VPP")
  field(ONST, "VRMS")
  field(TWST, "DBM")
}


record(mbbo, "$(DEVICE):$(SOURCE):UNIT:VOLTAGE") {
  field(DTYP, "stream")
  field(OUT,  "@keysight-33500x.proto src_write_string($(SOURCE),VOLT:UNIT) $(PORT)")
  field(FLNK, "$(DEVICE):$(SOURCE):UNIT:VOLTAGE:RBV")
  field(ZRST, "Vpp")
  field(ONST, "Vrms")
  field(TWST, "dBm")
}


record(sseq, "$(DEVICE):$(SOURCE):iUPDATEAMP") {
  field(DOL1, "$(DEVICE):$(SOURCE):UNIT:VOLTAGE:RBV CP")
  field(DOL2, "$(DEVICE):$(SOURCE):UNIT:VOLTAGE:RBV")
  field(DOL3, "$(DEVICE):$(SOURCE):UNIT:VOLTAGE:RBV")
  field(DOL4, "$(DEVICE):$(SOURCE):UNIT:VOLTAGE:RBV")

  field(LNK1, "$(DEVICE):$(SOURCE):AMP.EGU")
  field(LNK2, "$(DEVICE):$(SOURCE):AMP:MIN.EGU PP")
  field(LNK3, "$(DEVICE):$(SOURCE):AMP:MAX.EGU")
  field(LNK4, "$(DEVICE):$(SOURCE):AMP:RBV.EGU PP")
}


record(ao, "$(DEVICE):$(SOURCE):OFFSET") {
  field(DESC, "Offset")
  field(OUT,  "$(DEVICE):$(SOURCE):iOFFSET PP")
  field(PREC, "3")
  field(EGU,  "mV")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(stringout, "$(DEVICE):$(SOURCE):iOFFSET") {
  field(DESC, "Offset")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):OFFSET:RBV")
  field(OUT,  "@keysight-33500x.proto src_write_string($(SOURCE),VOLT:OFFS) $(PORT)")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(mbbo, "$(DEVICE):$(SOURCE):OFFSET:PREDEF") {
  field(DESC, "Predefined Offset")
  field(ZRST, "MINIMUM")
  field(ONST, "MAXIMUM")
  field(TWST, "DEFAULT")
  field(FLNK, "$(DEVICE):$(SOURCE):OFFSET:iPREDEF2STR")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(stringout, "$(DEVICE):$(SOURCE):OFFSET:iPREDEF2STR") {
  field(DOL,  "$(DEVICE):$(SOURCE):OFFSET:PREDEF")
  field(OUT,  "$(DEVICE):$(SOURCE):iOFFSET PP")
  field(OMSL, "closed_loop")
}


record(ai, "$(DEVICE):$(SOURCE):OFFSET:MIN") {
  field(DESC, "Minimum Offset")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):OFFSET:MAX")
  field(INP,  "@keysight-33500x.proto src_read_float_param($(SOURCE),VOLT:OFFS,MIN) $(PORT)")
  field(PREC, "3")
  field(EGU,  "mV")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(ai, "$(DEVICE):$(SOURCE):OFFSET:MAX") {
  field(DESC, "Maximum Offset")
  field(DTYP, "stream")
  field(FLNK, "$(DEVICE):$(SOURCE):OFFSET:iSETMINMAX")
  field(INP,  "@keysight-33500x.proto src_read_float_param($(SOURCE),VOLT:OFFS,MAX) $(PORT)")
  field(PREC, "3")
  field(EGU,  "mV")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}


record(seq, "$(DEVICE):$(SOURCE):OFFSET:iSETMINMAX") {
  field(DESC, "Set offset PV parameters")
  field(DOL1, "$(DEVICE):$(SOURCE):OFFSET:MIN")
  field(LNK1, "$(DEVICE):$(SOURCE):OFFSET.DRVL")
  field(DOL2, "$(DEVICE):$(SOURCE):OFFSET:MIN")
  field(LNK2, "$(DEVICE):$(SOURCE):OFFSET.LOPR")
  field(DOL3, "$(DEVICE):$(SOURCE):OFFSET:MAX")
  field(LNK3, "$(DEVICE):$(SOURCE):OFFSET.DRVH")
  field(DOL4, "$(DEVICE):$(SOURCE):OFFSET:MAX")
  field(LNK4, "$(DEVICE):$(SOURCE):OFFSET.HOPR")
}


record(ai, "$(DEVICE):$(SOURCE):OFFSET:RBV") {
  field(DESC, "Offset Readback")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_float($(SOURCE),VOLT:OFFS) $(PORT)")
  field(PREC, "3")
  field(EGU,  "mV")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}

###################################
### Arbitrary Waveform Function ###
###################################

record(stringin, "$(DEVICE):$(SOURCE):ARBFILENAME") {
  field(DESC, "Arbitrary waveform name")
}

record(stringout, "$(DEVICE):$(SOURCE):ARBWAVENAME") {
  field(DESC, "Select Arb. waveform name")
  field(DTYP, "stream")
  field(OUT,  "@keysight-33500x.proto src_write_string($(SOURCE),FUNC:ARB) $(PORT)")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
  field(FLNK, "$(DEVICE):$(SOURCE):ARBWAVENAME:RBV")
}

record(stringin, "$(DEVICE):$(SOURCE):ARBWAVENAME:RBV") {
  field(DESC, "Current Arb. waveform name")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_string($(SOURCE),FUNC:ARB) $(PORT)")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}

record(waveform, "$(DEVICE):$(SOURCE):ARBDATA_DAC") {
  field(DESC, "Arbitrary int waveform data input")
  field(DTYP, "stream")
  field(FTVL, "LONG")
  field(NELM, "65536")
  field(INP,  "@keysight-33500x.proto src_write_int_array($(SOURCE),DATA:ARB:DAC,$(DEVICE):$(SOURCE):ARBFILENAME) $(PORT)")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}

record(waveform, "$(DEVICE):$(SOURCE):ARBDATA") {
  field(DESC, "Arbitrary float waveform data input")
  field(DTYP, "stream")
  field(FTVL, "DOUBLE")
  field(PREC, "3")
  field(NELM, "65536")
  field(INP,  "@keysight-33500x.proto src_write_float_array($(SOURCE),DATA:ARB,$(DEVICE):$(SOURCE):ARBFILENAME) $(PORT)")
  field(SDIS, "$(DEVICE):$(SOURCE):DISABLE")
}

record(ao, "$(DEVICE):$(SOURCE):ARBSAMPRATE") {
  field(DESC, "Set Arbitrary waveform sampling rate")
  field(DTYP, "stream")
  field(OUT,  "@keysight-33500x.proto src_write_float($(SOURCE),FUNC:ARB:SRAT) $(PORT)")
  field(PREC, "6")
  field(EGU,  "Sa/s")
  field(DRVH, "250000000")
  field(DRVL, "0.000001")
  field(FLNK, "$(DEVICE):$(SOURCE):ARBSAMPRATE:RBV")
}

record(ai, "$(DEVICE):$(SOURCE):ARBSAMPRATE:RBV") {
  field(DESC, "Read Arbitrary waveform sampling rate")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_float($(SOURCE),FUNC:ARB:SRAT) $(PORT)")
  field(PREC, "6")
  field(EGU,  "Sa/s")
}

record(mbbo, "$(DEVICE):$(SOURCE):ARBFILT") {
  field(DESC, "Set Arbitrary waveform filter type")
  field(ZRST, "OFF")
  field(ONST, "STEP")
  field(TWST, "NORM")
  field(DTYP, "stream")
  field(OUT,  "@keysight-33500x.proto src_write_string($(SOURCE),FUNC:ARB:FILT) $(PORT)")
  field(FLNK, "$(DEVICE):$(SOURCE):ARBFILT:RBV")
}

record(mbbi, "$(DEVICE):$(SOURCE):ARBFILT:RBV") {
  field(DESC, "Read Arbitrary waveform filter type")
  field(PINI, "YES")
  field(ZRST, "OFF")
  field(ONST, "STEP")
  field(TWST, "NORM")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_string($(SOURCE),FUNC:ARB:FILT) $(PORT)")
}

record(ao, "$(DEVICE):$(SOURCE):ARBFREQ") {
  field(DESC, "Set Arbitrary waveform frequency")
  field(DTYP, "stream")
  field(OUT,  "@keysight-33500x.proto src_write_float($(SOURCE),FUNC:ARB:FREQ) $(PORT)")
  field(PREC, "6")
  field(EGU,  "Hz")
  field(FLNK, "$(DEVICE):$(SOURCE):ARBFREQ:RBV")
}

record(ai, "$(DEVICE):$(SOURCE):ARBFREQ:RBV") {
  field(DESC, "Read Arbitrary waveform frequency")
  field(DTYP, "stream")
  field(PINI, "YES")
  field(INP,  "@keysight-33500x.proto src_read_float($(SOURCE),FUNC:ARB:FREQ) $(PORT)")
  field(PREC, "6")
  field(EGU,  "Hz")
}

record(ao, "$(DEVICE):$(SOURCE):ARBPERIOD") {
  field(DESC, "Set Arbitrary waveform period")
  field(DTYP, "stream")
  field(OUT,  "@keysight-33500x.proto src_write_float($(SOURCE),FUNC:ARB:PER) $(PORT)")
  field(PREC, "6")
  field(EGU,  "s")
  field(FLNK, "$(DEVICE):$(SOURCE):ARBPERIOD:RBV")
}

record(ai, "$(DEVICE):$(SOURCE):ARBPERIOD:RBV") {
  field(DESC, "Read Arbitrary waveform period")
  field(PINI, "YES")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_float($(SOURCE),FUNC:ARB:PER) $(PORT)")
  field(PREC, "6")
  field(EGU,  "s")
}

record(ai, "$(DEVICE):$(SOURCE):ARBPOINTS:RBV") {
  field(DESC, "Read Arb waveform point count")
  field(PINI, "YES")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto src_read_int($(SOURCE),FUNC:ARB:POIN) $(PORT)")
  field(EGU,  "points")
}

record(ao, "$(DEVICE):$(SOURCE):ARBPEAKV") {
  field(DESC, "Set Arb peak to peak voltage")
  field(DTYP, "stream")
  field(OUT,  "@keysight-33500x.proto src_write_float($(SOURCE),FUNC:ARB:PTP) $(PORT)")
  field(PREC, "6")
  field(EGU,  "V")
  field(FLNK, "$(DEVICE):$(SOURCE):ARBPEAKV:RBV")
}

record(ai, "$(DEVICE):$(SOURCE):ARBPEAKV:RBV") {
  field(DESC, "Read Arb peak to peak voltage")
  field(DTYP, "stream")
  field(PINI, "YES")
  field(INP,  "@keysight-33500x.proto src_read_float($(SOURCE),FUNC:ARB:PTP) $(PORT)")
  field(PREC, "6")
  field(EGU,  "V")
}

record(bo, "$(DEVICE):$(SOURCE):ARBSYNC") {
  field(DESC, "Synchronize arbitrary waves")
  field(DTYP, "stream")
  field(ZNAM, "Sync")
  field(OUT,  "@keysight-33500x.proto src_write_command($(SOURCE),FUNC:ARB:SYNC) $(PORT)")
}

############################
#####  Misc commands  ######
############################

record(bo, "$(DEVICE):$(SOURCE):MEMCLEAR") {
  field(DESC, "Clear Volatile memory on channel")
  field(ONAM, "CLEAR")
  field(DTYP, "stream")
  field(OUT,  "@keysight-33500x.proto src_write_command($(SOURCE),DATA:VOL:CLE) $(PORT)")
}
