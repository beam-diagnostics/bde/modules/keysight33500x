record(mbbi, "$(DEVICE):UNIT:ANGLE:RBV") {
  field(DESC, "Get the unit of angle")
  field(DTYP, "stream")
  field(INP,  "@keysight-33500x.proto read_string(UNIT:ANGL) $(PORT)")
  field(ZRST, "DEG")
  field(ONST, "RAD")
  field(TWST, "SEC")
}


record(mbbo, "$(DEVICE):UNIT:ANGLE") {
  field(DESC, "Set the unit of angle")
  field(DTYP, "stream")
  field(OUT,  "@keysight-33500x.proto write_string(UNIT:ANGL) $(PORT)")
  field(FLNK, "$(DEVICE):UNIT:ANGLE:RBV")
  field(ZRST, "DEG")
  field(ONST, "RAD")
  field(TWST, "SEC")
  field(THST, "DEF")
}

# Using waveform record because the error message can be larger than 30 chars
record(waveform, "$(DEVICE):ERROR") {
  field(DESC, "System error message")
  field(DTYP, "stream")
  field(FTVL, "STRING")
  field(NELM, "200")
  field(INP, "@keysight-33500x.proto read_string_raw(SYST:ERR) $(PORT)")
}

record(waveform, "$(DEVICE):DISP:TEXT") {
  field(DESC, "Display message on display")
  field(DTYP, "stream")
  field(FTVL, "STRING")
  field(NELM, "40")
  field(INP, "@keysight-33500x.proto write_string(DISP:TEXT) $(PORT)")
}

record(bo, "$(DEVICE):DISP:CLEAR") {
  field(DESC, "Clear display message")
  field(DTYP, "stream")
  field(ONAM, "CLEAR")
  field(OUT,  "@keysight-33500x.proto write_command(DISP:TEXT:CLE) $(PORT)")
}

record(bo, "$(DEVICE):DISP:CTL") {
  field(DESC, "Control display (on/off)")
  field(DTYP, "stream")
  field(ZNAM, "OFF")
  field(ONAM, "ON")
  field(OUT,  "@keysight-33500x.proto write_command(DISP) $(PORT)")
}

record(mbbo, "$(DEVICE):DISP:ARBUNIT") {
  field(DESC, "Set arb wvf rate unit")
  field(DTYP, "stream")
  field(OUT,  "@keysight-33500x.proto write_string(DISP:UNIT:ARBR) $(PORT)")
  field(FLNK, "$(DEVICE):DISP:ARBUNIT:RBV")
  field(ZRST, "SRAT")
  field(ONST, "FREQ")
  field(TWST, "PER")
}

record(mbbi, "$(DEVICE):DISP:ARBUNIT:RBV") {
  field(DESC, "Read arb wvf rate unit")
  field(DTYP, "stream")
  field(PINI, "YES")
  field(INP,  "@keysight-33500x.proto read_string(DISP:UNIT:ARBR) $(PORT)")
  field(ZRST, "SRAT")
  field(ONST, "FREQ")
  field(TWST, "PER")
}

record(stringout, "$(DEVICE):CMD") {
  field(DESC, "Send a custom SCPI command to device")
  field(DTYP, "stream")
  field(OUT,  "@keysight-33500x.proto sendCmd $(PORT)")
}

###################################################################################


record(asyn, "$(DEVICE):ASYN") {
  field(DTYP, "asynRecordDevice")
  field(PORT, "$(PORT)")
#  field(OEOS, "\n")
#  field(IEOS, "\n")
}


record(bi, "$(DEVICE):ConnectedR") {
  field(DESC, "Connection state")
  field(INP,  "$(DEVICE):ASYN.CNCT CP")
  field(ONAM, "Connected")
  field(ZNAM, "Disconnected")
  field(ZSV,  "MAJOR")
}


record(calcout, "$(DEVICE):iConnectEventGen") {
  field(DESC, "Generate connection 'event'")
  field(INPA, "$(DEVICE):ASYN.CNCT CP")
  field(VAL,  "0")
  field(CALC, "A")
  field(OOPT, "Transition To Non-zero")
  field(OUT,  "$(DEVICE):iConnectEvent.PROC")
}


#
# Can use this record to detect when the device (re)connects
#
record(calc, "$(DEVICE):iConnectEvent") {
  field(DESC, "Number of (re)connections")
  field(INPA, "$(DEVICE):iConnectEvent")
  field(CALC, "A+1")
  field(FLNK, "$(DEVICE):iConnectEvent-FO")
  field(VAL,  "0")
}


record(fanout, "$(DEVICE):iConnectEvent-FO") {
  field(LNK1, "$(DEVICE):UNIT:ANGLE:RBV")
}
