# Scripts

## generator_config.py

This python3 script can be used to parse a waveform data file and load it to the generator, setting all the needed parameters.

### Needed modules

- Scipy
- Numpy
- PyEpics

### Usage

```
  python3 generator_config --generator "LAB:WFG-002:" --output 1 --samp_rate 10e6 --amplitude 0.2 waveform.data
```
