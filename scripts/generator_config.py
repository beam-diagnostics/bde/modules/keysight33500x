#!/usr/bin/env python3
import argparse
import numpy as np
import scipy.signal
from epics import caget, caput

MAX_SAMPLES = 65536

# Command line arguments parsing
parser = argparse.ArgumentParser()
parser.add_argument('waveform', help='Waveform file')
parser.add_argument('-g', '--generator', help='Generator PV Prefix', default='LAB:WFG-006:')
parser.add_argument('-o', '--output', type=int, help='Generator output selection', default=1)
parser.add_argument('-s', '--samp_rate', type=float, help='Waveform sampling rate', default=6.25e6)
parser.add_argument('-a', '--amplitude', type=float, help='Output signal amplitude [Vpp]', default=0.5)
parser.add_argument('-c', '--clear', action='store_true', help='Clear generator volatile memory before sending the new waveform', default=False)
args = parser.parse_args()

output_str = str(args.output)+':'

#Create waveform generator PV strings
genPV = {
    "output" : "OUT",
    "arbfilename" : "ARBFILENAME",
    "arbwavename" : "ARBWAVENAME",
    "arbdata" : "ARBDATA",
    "arbfreq" : "ARBFREQ",
    "arbperiod" : "ARBPERIOD",
    "amplitude" : "AMP",
    "function" : "FUNC",
    "arbsamprate" : "ARBSAMPRATE",
    "memclear" : "MEMCLEAR",
}
genPV = {k: args.generator+output_str+v for k, v in genPV.items()}

#Load waveform
wave_name = args.waveform.split('.')[0]
if len(wave_name) > 12:
    print("The waveform name is longer than 12 chars, it will be trimmed down to be sent to the generator")
    wave_name = wave_name[:12]

wave_data = np.loadtxt(args.waveform)

#Original wave period
wave_period = float(wave_data.size)/args.samp_rate

#Downsample the wave if necessary
if wave_data.size > MAX_SAMPLES :
    print("Downsampling wave from "+str(wave_data.size)+" to "+str(MAX_SAMPLES)+" samples")
    wave_data = scipy.signal.resample(wave_data, MAX_SAMPLES)

#Scale from -1.0 to 1.0
wave_scaled = wave_data
wave_scaled /= np.max(np.abs(wave_scaled),axis=0)

#Configure generator
caput(genPV['output'], 'Off')
if args.clear:
    caput(genPV['memclear'], 1)
caput(genPV['arbfilename'], wave_name)
caput(genPV['arbdata'], wave_scaled)
caput(genPV['arbwavename'], wave_name)
caput(genPV['arbperiod'], wave_period)
caput(genPV['amplitude'], args.amplitude)
caput(genPV['function'], 'ARB')
caput(genPV['output'], 'On')
